from mongoengine import *

class Customers(Document):
    cust_id = StringField(required=True, unique=True)    
    name = StringField(required=True)
    emailid = StringField(required=True, unique=True)
    password = StringField(required=True)
    subscription = StringField(required=True)
    renewal_date = StringField(required=True)


class Plans(Document):
    plan_id = StringField(required=True, unique=True)
    name = StringField(required=True, unique=True)
    price = IntField(required=True)
    no_of_websites = IntField(required=True)


class Websites(Document):
    website_id = StringField(required=True, unique=True)
    url = StringField(required=True, unique=True)
    cust_id = ListField(required=True)