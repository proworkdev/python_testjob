from mongoengine import connect

class DB(object):
    def __init__(self):
        mongo_host = 'localhost'
        mongo_port = 27017
        connect('test_job', host=mongo_host, port=mongo_port)