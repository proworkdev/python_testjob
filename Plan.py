from Models.Model import Plans
from db import DB


class Plan(object):
    def __init__(self, plan_id):
        self.plan_id = plan_id    
        self.name = None    
        self.price = None
        self.no_of_websites = None
        self.db = None
        self.get_options()

    def get_plan_options(self, plan_id):
        plan_list = Plans.objects(plan_id=plan_id)
        if not plan_list:
            return None
        plan = plan_list[0]
        options = {
            'plan_id': plan_id,
            'name': plan.name,
            'price': plan.price,
            'no_of_websites': plan.no_of_websites
        }
        return options

    def get_options(self):
        db = DB() # to make the connection
        self.options = self.get_plan_options(self.plan_id)
        self.plan_id =  self.plan_id    
        self.name = self.options.name
        self.price = self.options.price
        self.no_of_websites = self.options.no_of_websites