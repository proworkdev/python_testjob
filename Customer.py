from Models.Model import Customers
from db import DB

class Customer(object):
    def __init__(self, cust_id):
        self.cust_id = cust_id    
        self.name = None
        self.emailid = None
        self.password = None
        self.subscription = None
        self.renewal_date = None
        self.options = None
        self.db = None
        self.get_options()

    def get_customer_options(self, customer_id):
        customer_list = Customers.objects(cust_id=customer_id)
        if not customer_list:
            return None
        customer = customer_list[0]
        options = {
            'cust_id': customer_id,
            'name': customer.name,
            'emailid': customer.emailid,
            'password': customer.password,
            'subscription': customer.subscription,
            'renewal_date': customer.renewal_date
        }
        return options

    def get_options(self):
        DB() # to make the connection
        self.options = self.get_customer_options(self.cust_id)
        self.cust_id =  self.cust_id    
        self.name = self.options.name
        self.emailid = self.options.emailid
        self.password = self.options.password
        self.subscription = self.options.subscription
        self.renewal_date = self.options.renewal_date