from Customer import Customer
from Plan import Plan
from Website import Website
import jinja2

# get the customer details
cust_id = input("Enter Customer ID ")
customer = Customer(cust_id = cust_id)
customer_plan = customer.subscription

# get the plan details for the current customer
plan = Plan(plan_id = customer_plan)

# get the website for the current customer
website_id = input('Enter Website ID ')
website = Website(website_id = website_id)



