from Models.Model import Websites
from db import DB

class Website(object):
    def __init__(self, website_id):
        self.website_id = website_id    
        self.url = None    
        self.cust_id = None
        self.db = None
        self.get_options()

    def get_website_options(self, website_id):
        website_list = Websites.objects(website_id=website_id)
        if not website_list:
            return None
        website = website_list[0]
        options = {
            'website_id': website_id,
            'url': website.name,
            'cust_id': website.price
        }
        return options

    def get_options(self):
        db = DB() # to make the connection
        self.options = self.get_website_options(self.website_id)
        self.website_id =  self.website_id    
        self.url = self.options.url
        self.cust_id = self.options.cust_id